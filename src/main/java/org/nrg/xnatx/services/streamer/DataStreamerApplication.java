package org.nrg.xnatx.services.streamer;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xnat.web.http.StreamingResponseCompletedEvent;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.nrg.xnatx.services.streamer.configuration.OperationOptions.CATALOG;

@SpringBootApplication
@EnableWebSecurity
@Slf4j
public class DataStreamerApplication extends WebSecurityConfigurerAdapter implements ApplicationRunner, ApplicationListener<StreamingResponseCompletedEvent> {
    public static void main(final String[] arguments) {
        final SpringApplication application = new SpringApplication(DataStreamerApplication.class);
        final Integer           port        = getPort(arguments);
        if (port != null) {
            application.setDefaultProperties(Collections.singletonMap("server.port", port));
        }
        application.run(arguments);
    }

    @Override
    public void onApplicationEvent(final StreamingResponseCompletedEvent event) {
        System.out.println("The streaming response has completed, tried to send data from " + event.getMap().toString());
        if (event.getHistory() == null) {
            System.out.println("No history was recorded.");
        } else {
            System.out.println("The write history can be found at " + event.getHistory().toString());
        }
        final ApplicationContext context = getApplicationContext();
        if (context instanceof ConfigurableApplicationContext) {
            ((ConfigurableApplicationContext) context).close();
        }
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.authorizeRequests().anyRequest().permitAll();
    }

    @Override
    public void run(final ApplicationArguments arguments) {
        if (arguments.containsOption("help")) {
            showHelpBanner();
        }

        if (!arguments.containsOption(CATALOG)) {
            showHelpBanner(-1, "You must specify the --catalog=<path> argument to start this service.");
        }

        final List<String> values = arguments.getOptionValues(CATALOG);
        if (values.isEmpty()) {
            showHelpBanner(-2, "You must specify the --catalog=<path> argument to start this service.");
        }
        if (values.size() > 1) {
            log.warn("Multiple values were found for the --catalog argument, only returning first value: {}", StringUtils.join(values, ", "));
        }

        final File catalogPath = Paths.get(values.get(0)).toFile();
        if (!(catalogPath.exists() && catalogPath.isFile() && catalogPath.canRead())) {
            showHelpBanner(-3, "The value for catalogPath doesn't indicate an existing file that can be read: " + catalogPath.getAbsolutePath());
        }

        System.out.println("Starting application with the catalog \"" + catalogPath.getAbsolutePath() + "\"");
    }

    private static Integer getPort(final String[] arguments) {
        final boolean          containsShortOption = ArrayUtils.contains(arguments, "-p");
        final Optional<String> longOption          = Arrays.stream(arguments).filter(argument -> argument.startsWith("--port=")).findAny();
        final boolean          containsLongOption  = longOption.isPresent();
        return !(containsShortOption || containsLongOption) ? null : Integer.parseInt(containsShortOption ? arguments[ArrayUtils.indexOf(arguments, "-p") + 1] : StringUtils.removeStart(longOption.get(), "--port="));
    }

    private static void showHelpBanner() {
        showHelpBanner(0, null);
    }

    private static void showHelpBanner(final int status, final String message) {
        if (StringUtils.isNotBlank(message)) {
            System.out.println(message);
            System.out.println();
        }
        System.out.println("XNAT Data Streamer");
        System.out.println("Usage:");
        System.out.println(" $ streamer --" + CATALOG + "=/path/to/cached/resource/map/catalogId.json");
        System.exit(status);
    }
}
