package org.nrg.xnatx.services.streamer.controllers;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xnat.web.http.ResourceMapZipStreamingResponseBody;
import org.nrg.xnatx.services.streamer.configuration.OperationOptions;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.IOException;
import java.nio.file.Paths;

import static org.nrg.xnat.web.http.AbstractZipStreamingResponseBody.MEDIA_TYPE;

@RestController
@RequestMapping("/download")
@Data
@Accessors(prefix = "_")
@Slf4j
public class DataStreamerController {
    public DataStreamerController(final OperationOptions options, final ApplicationEventPublisher publisher) {
        log.debug("Creating data streamer controller with the options: {}", options);
        _options = options;
        _publisher = publisher;
    }

    @GetMapping(value = "{catalogId}", produces = MEDIA_TYPE)
    @ResponseBody
    public ResponseEntity<StreamingResponseBody> downloadSessionCatalogZip(@PathVariable final String catalogId) throws IOException {
        log.debug("Got a request to download {}", catalogId);
        if (!StringUtils.contains(_options.getCatalog(), catalogId)) {
            throw new BadCredentialsException("Invalid catalog ID specified");
        }
        return ResponseEntity.ok()
                             .header(HttpHeaders.CONTENT_TYPE, MEDIA_TYPE)
                             .header(HttpHeaders.CONTENT_DISPOSITION, getAttachmentDisposition(catalogId, "zip"))
                             .body(new ResourceMapZipStreamingResponseBody(Paths.get(_options.getCatalog()), getPublisher()));
    }

    /**
     * Gets the attachment disposition for the specified filename. This is a standard format that looks like:
     *
     * <pre>attachment; filename="filename.txt"</pre>
     * <p>
     * This method accepts multiple strings, which it just concatenates until the last string, which is appended
     * to the previously concatenated strings after a dot (".") is appended. For example, if you called this method
     * with the strings "foo", "bar", and "txt", the resulting filename would be "foobar.txt". If you specify a
     * single string, no dot is added.
     *
     * @param parts The filename parts to concatenate and format.
     *
     * @return The filename formatted as an attachment disposition.
     */
    private static String getAttachmentDisposition(final String... parts) {
        final int    maxIndex = parts.length - 1;
        final String filename = maxIndex == 0 ? parts[0] : StringUtils.join(ArrayUtils.subarray(parts, 0, maxIndex)) + "." + parts[maxIndex];
        return String.format(ATTACHMENT_DISPOSITION, filename);
    }

    private static final String ATTACHMENT_DISPOSITION = "attachment; filename=\"%s\"";

    private final OperationOptions          _options;
    private final ApplicationEventPublisher _publisher;
}
