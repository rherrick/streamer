package org.nrg.xnatx.services.streamer.configuration;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
@Accessors(prefix = "_")
public class OperationOptions {
    public static final String CATALOG = "catalog";

    public OperationOptions() {
        _catalog = null;
    }

    @Value("${catalog:}")
    private final String _catalog;
}
